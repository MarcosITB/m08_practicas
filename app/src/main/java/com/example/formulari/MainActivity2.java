package com.example.formulari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {


    private Button boton2;
    private SeekBar edad;
    private RadioButton hola;
    private TextView progreso;
    boolean estado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        boton2 = findViewById(R.id.boton2);
        edad = findViewById(R.id.edad);
        hola = findViewById(R.id.hola);
        progreso = findViewById(R.id.progreso);


        final Bundle bundle = getIntent().getExtras();
        final String nombre = bundle.getString("nom");

        edad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progreso.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (MainActivity2.this, MainActivity3.class);
                // intent.putExtra("nom", nombre.getText());
                estado = hola.isChecked();
                if (edad.getProgress() < 18 || edad.getProgress() > 60) {
                    Context context = getApplicationContext();
                    CharSequence text = "Hello toast!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, "inserta edad entre 18 y 60", duration);

                    toast.show();
                } else {
                    intent.putExtra("edad", edad.getProgress());
                    intent.putExtra("nombre", nombre);
                    intent.putExtra("saludo", estado);
                    startActivity(intent);
                }

            }
        });
    }
}