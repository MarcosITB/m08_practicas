package com.example.formulari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity {

    private Button mostrar;
    private Button compartir;
    private String mensaje;
    private Intent email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        mostrar = findViewById(R.id.mostrar);
        compartir = findViewById(R.id.compartir);


        final Bundle bundle = getIntent().getExtras();
        final String nombre2 = bundle.getString("nombre");
        final boolean estado = bundle.getBoolean("saludo");
        final int edad = bundle.getInt("edad");


        mostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (estado == false) {
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "Espero tornar a veure't  " + nombre2 + ", abans de que facis " + edad + " anys", duration);

                    mensaje = "Espero tornar a veure't  " + nombre2 + ", abans de que facis " + edad + " anys";
                    toast.show();

                }
                if (estado == true) {
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, "Hola " + nombre2 + ", com portes aquests " + edad + " anys?", duration);
                    mensaje = "Hola " + nombre2 + ", com portes aquests " + edad + " anys?";
                    toast.show();
                }

            }
        });
        compartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{"marcos.castanyo.7e3@itb.cat"});
                email.putExtra(Intent.EXTRA_SUBJECT, "Formulari");
                email.putExtra(Intent.EXTRA_TEXT, mensaje);

                email.setType("message/rfc822");

                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });
    }
}